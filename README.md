# better-secrets (bs)

symmetric file encryption using libsodium

bs stands for better secrets, but there are a lot of good secret stores out
there already, so it's kind of bs.

we define and implement a few things:

- some container structs for encrypted data and password-protected keys, based
  on libsodium's `secret_box` and `pwhash` primitives (`bs-containers` crate)
- some functions for creating and unlocking the keys, and using them to
  encrypt/decrypt data (`bs-crypto` crate)
- a cli program to wrap these functions, optionally using an agent daemon (`bs`
  crate)
- a client-server request/response protocol for communicating to a stateful
  agent daemon, which caches unlocked keys (`bs-transport` and `bs-messages`
  crates)
- a program which runs the agent daemon (`bs-agent` crate)

## todo

- make the readme good
  - write a user-facing bit
  - how to build, run tests
  - style advice for developers
- remaining actual functionality
  - having the agent create its own socket in a temporary directory
- integration tests
  - key generation [done]
  - encryption solo [done]
  - encryption round trip
  - decryption solo
  - agent stuff
- unit tests
  - on crypto crate
  - on transport crate
- things to reconsider/iron out:
  - protocol spec, re: versioning
  - key interchange format
  - CLI syntax
  - asymmetric encryption???
- unsolved problems
  - agent forwarding across SSH?
- ci system with some nice badges
- rustdocs?
- man pages
- separate transport crate into its own project?

## bs-agent

a daemon providing a stateful keystore, with access gated behind calling out to
`ssh-askpass` for user confirmation

it provides the following functions:
- adding a key to the store
- encrypting or decrypting data, using a key in the store

it does **not** ever provide a key back to the client. the intended usage is
that the client sends encrypted data to the daemon and receives back decrypted
data.
