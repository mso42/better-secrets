use crate::libsodium::memory_protection::MemoryProtectedBytes;
use crate::CryptoError;
use crate::HashParameters;

use core::ffi::c_void;
use std::convert::TryInto;
use std::os::raw::c_char;
use std::os::raw::c_uchar;

use libsodium_sys as ls;
use serde::{Deserialize, Serialize};

pub const PWHASH_SALTBYTES: usize = ls::crypto_pwhash_SALTBYTES as usize;

#[derive(Debug, PartialEq, Eq, Hash, Clone, Serialize, Deserialize)]
pub struct Salt(pub [u8; PWHASH_SALTBYTES]);

impl Salt {
    pub fn random() -> Self {
        let mut buf = [0u8; PWHASH_SALTBYTES];
        unsafe {
            ls::randombytes_buf((&mut buf).as_mut_ptr() as *mut c_void, PWHASH_SALTBYTES);
        }
        Self(buf)
    }
}

pub fn pwhash_into(
    password: MemoryProtectedBytes,
    salt: &Salt,
    hash_parameters: &HashParameters,
    target: &mut [u8],
) -> Result<(), CryptoError> {
    let (opslimit, memlimit) = match hash_parameters {
        HashParameters::Sensitive => (
            ls::crypto_pwhash_OPSLIMIT_SENSITIVE,
            ls::crypto_pwhash_MEMLIMIT_SENSITIVE,
        ),
        HashParameters::Interactive => (
            ls::crypto_pwhash_OPSLIMIT_INTERACTIVE,
            ls::crypto_pwhash_MEMLIMIT_INTERACTIVE,
        ),
    };
    unsafe {
        let pwhash_result = ls::crypto_pwhash(
            target.as_mut_ptr() as *mut c_uchar,
            target.len() as u64,
            password.as_ptr() as *const c_char,
            password.len().try_into().unwrap(),
            salt.0.as_ptr(),
            opslimit.into(),
            memlimit.try_into().unwrap(),
            ls::crypto_pwhash_ALG_DEFAULT.try_into().unwrap(),
        );
        if pwhash_result != 0 {
            ls::sodium_memzero(target.as_mut_ptr() as *mut c_void, target.len());
            Err(CryptoError::SodiumError(
                "pwhash failed - out of memory?".into(),
            ))
        } else {
            Ok(())
        }
    }
}
