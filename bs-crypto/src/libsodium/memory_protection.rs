use core::ffi::c_void;
use libsodium_sys as ls;
use std::ops::{Deref, DerefMut};
use std::slice::{from_raw_parts, from_raw_parts_mut};

pub struct MemoryProtectedBytes {
    ptr: *mut u8,
    len: usize,
}

unsafe impl Send for MemoryProtectedBytes {}
unsafe impl Sync for MemoryProtectedBytes {}

impl Drop for MemoryProtectedBytes {
    fn drop(&mut self) {
        unsafe {
            ls::sodium_memzero(self.ptr as *mut c_void, self.len);
            ls::sodium_free(self.ptr as *mut c_void);
        }
    }
}

impl MemoryProtectedBytes {
    pub fn new(len: usize) -> Self {
        unsafe {
            let ptr = ls::sodium_malloc(len) as *mut u8;
            if ptr.is_null() {
                panic!("Could not allocate protected memory");
            }
            Self { ptr, len }
        }
    }
}

impl Deref for MemoryProtectedBytes {
    type Target = [u8];

    fn deref(&self) -> &Self::Target {
        unsafe { from_raw_parts(self.ptr, self.len) }
    }
}

impl DerefMut for MemoryProtectedBytes {
    fn deref_mut(&mut self) -> &mut [u8] {
        unsafe { from_raw_parts_mut(self.ptr, self.len) }
    }
}

impl std::fmt::Debug for MemoryProtectedBytes {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("MemoryProtectedBytes")
            .finish_non_exhaustive()
    }
}

impl From<String> for MemoryProtectedBytes {
    fn from(string: String) -> Self {
        string.into_bytes().into()
    }
}

impl From<Box<[u8]>> for MemoryProtectedBytes {
    fn from(boxed_slice: Box<[u8]>) -> Self {
        boxed_slice.into_vec().into()
    }
}

impl From<Vec<u8>> for MemoryProtectedBytes {
    fn from(mut vec: Vec<u8>) -> Self {
        let mut out = MemoryProtectedBytes::new(vec.len());
        let out_slice: &mut [u8] = &mut *out;
        out_slice.copy_from_slice(&vec);
        unsafe {
            ls::sodium_memzero(vec.as_mut_ptr() as *mut c_void, vec.capacity());
        }
        out
    }
}

impl PartialEq for MemoryProtectedBytes {
    fn eq(&self, other: &Self) -> bool {
        if self.len != other.len {
            return false;
        }
        unsafe {
            let cmp_res = ls::sodium_memcmp(
                self.ptr as *const c_void,
                other.ptr as *const c_void,
                self.len,
            );
            cmp_res == 0
        }
    }
}
