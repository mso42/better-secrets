use core::ffi::c_void;
use libsodium_sys as ls;
use serde::{Deserialize, Serialize};
use std::convert::TryInto;

use crate::libsodium::memory_protection::MemoryProtectedBytes;
use crate::libsodium::pwhash::{pwhash_into, Salt};
use crate::{CryptoError, HashParameters};

pub const SECRETBOX_KEYBYTES: usize = ls::crypto_secretbox_KEYBYTES as usize;

#[derive(Debug)]
pub struct Key {
    key_bytes: MemoryProtectedBytes,
}

impl Key {
    pub fn new_random() -> Self {
        let mut key_bytes = MemoryProtectedBytes::new(SECRETBOX_KEYBYTES);
        let ptr: *mut u8 = (&mut *key_bytes).as_mut_ptr();
        unsafe {
            ls::randombytes_buf(ptr as *mut c_void, SECRETBOX_KEYBYTES);
        }
        Key { key_bytes }
    }

    pub fn new_from_password(
        password: MemoryProtectedBytes,
        salt: &Salt,
        hash_parameters: &HashParameters,
    ) -> Result<Self, CryptoError> {
        let mut key_bytes = MemoryProtectedBytes::new(SECRETBOX_KEYBYTES);
        pwhash_into(password, salt, hash_parameters, &mut key_bytes)?;
        Ok(Key { key_bytes })
    }

    pub fn new_empty() -> Self {
        let key_bytes = MemoryProtectedBytes::new(SECRETBOX_KEYBYTES);
        Key { key_bytes }
    }
}

impl AsRef<[u8; SECRETBOX_KEYBYTES]> for Key {
    fn as_ref(&self) -> &[u8; SECRETBOX_KEYBYTES] {
        let slice: &[u8] = &*self.key_bytes;
        slice.try_into().unwrap()
    }
}

impl AsMut<[u8; SECRETBOX_KEYBYTES]> for Key {
    fn as_mut(&mut self) -> &mut [u8; SECRETBOX_KEYBYTES] {
        let slice: &mut [u8] = &mut *self.key_bytes;
        slice.try_into().unwrap()
    }
}

pub const SECRETBOX_NONCEBYTES: usize = ls::crypto_secretbox_NONCEBYTES as usize;

#[derive(Debug, PartialEq, Eq, Hash, Clone, Serialize, Deserialize)]
pub struct Nonce(pub [u8; SECRETBOX_NONCEBYTES]);

impl Nonce {
    pub fn random() -> Self {
        let mut buf = [0u8; SECRETBOX_NONCEBYTES];
        unsafe {
            ls::randombytes_buf((&mut buf).as_mut_ptr() as *mut c_void, SECRETBOX_NONCEBYTES);
        }
        Self(buf)
    }
}

pub const SECRETBOX_MACBYTES: usize = ls::crypto_secretbox_MACBYTES as usize;

#[derive(Debug, PartialEq, Eq, Hash, Clone, Serialize, Deserialize)]
pub struct Mac(pub [u8; SECRETBOX_MACBYTES]);

#[derive(Debug, PartialEq, Eq, Hash, Clone, Serialize, Deserialize)]
pub struct SecretBox {
    pub nonce: Nonce,
    pub mac: Mac,
    pub data: Box<[u8]>,
}

impl SecretBox {
    pub fn encrypt(message: &[u8], key: &Key) -> Self {
        let nonce = Nonce::random();
        let mut ciphertext = vec![0u8; message.len()].into_boxed_slice();
        let mut mac = Mac([0u8; SECRETBOX_MACBYTES]);
        unsafe {
            ls::crypto_secretbox_detached(
                ciphertext.as_mut_ptr(),
                mac.0.as_mut_ptr(),
                message.as_ptr(),
                message.len() as u64,
                nonce.0.as_ptr(),
                key.as_ref().as_ptr(),
            );
        }
        Self {
            nonce,
            mac,
            data: ciphertext,
        }
    }

    pub fn decrypt_into(&self, key: &Key, out: &mut [u8]) -> Result<(), CryptoError> {
        if self.data.len() != out.len() {
            return Err(CryptoError::InvalidBufferSize);
        }
        unsafe {
            let res = ls::crypto_secretbox_open_detached(
                out.as_mut_ptr(),
                self.data.as_ptr(),
                self.mac.0.as_ptr(),
                self.data.len() as u64,
                self.nonce.0.as_ptr(),
                key.as_ref().as_ptr(),
            );
            if res != 0 {
                Err(CryptoError::SodiumError("box decryption failed".into()))
            } else {
                Ok(())
            }
        }
    }
}
