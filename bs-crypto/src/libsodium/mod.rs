use crate::CryptoError;

pub mod memory_protection;
pub mod pwhash;
pub mod secretbox;

pub fn init() -> Result<(), CryptoError> {
    unsafe {
        let init_res = libsodium_sys::sodium_init();
        if init_res == -1 {
            Err(CryptoError::SodiumError("sodium_init() failed".into()))
        } else {
            Ok(())
        }
    }
}
