use crate::CryptoError;
use crate::Result;

pub fn encode(prefix: &str, comment: &Option<String>, data: impl AsRef<[u8]>) -> Result<String> {
    use CryptoError::*;
    let mut out = String::new();
    out.push_str(prefix);
    out.push('\n');
    if let Some(comment) = comment {
        if comment.contains('\n') {
            return Err(InvalidComment("Comment contained newline"));
        }
        out.push_str("untrusted comment: ");
        out.push_str(comment);
        out.push('\n');
    }
    out.push('\n');
    out.push_str(&base64::encode(data));
    out.push('\n');
    Ok(out)
}

pub fn decode(expected_prefix: &str, data: impl AsRef<str>) -> Result<Vec<u8>> {
    use CryptoError::*;
    let mut lines = data.as_ref().lines();
    let prefix = lines.next().ok_or(MissingSection)?;
    if prefix != expected_prefix {
        return Err(SignatureMismatch {
            expected: expected_prefix.to_string(),
            received: prefix.to_string(),
        });
    }
    while !lines.next().ok_or(MissingSection)?.is_empty() {
        // Skip line
    }
    let encoded_data = lines.next().ok_or(MissingSection)?;
    let decoded = base64::decode(encoded_data)?;
    Ok(decoded)
}
