use thiserror::Error;

#[derive(Debug, Error)]
pub enum CryptoError {
    #[error("libsodium error: {0}")]
    SodiumError(String),
    #[error(transparent)]
    EncodeError(#[from] rmp_serde::encode::Error),
    #[error(transparent)]
    DecodeError(#[from] rmp_serde::decode::Error),
    #[error("Signature mismatch: expected {expected}, received {received}")]
    SignatureMismatch { expected: String, received: String },
    #[error("Missing section")]
    MissingSection,
    #[error("Base64 decode error: {0}")]
    Base64DecodeError(#[from] base64::DecodeError),
    #[error("Invalid comment: {0}")]
    InvalidComment(&'static str),
    #[error("Invalid buffer size")]
    InvalidBufferSize,
}

pub type Result<T> = std::result::Result<T, CryptoError>;
