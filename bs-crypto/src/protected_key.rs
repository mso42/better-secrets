use crate::internal::printable_string_encoding::{decode, encode};
use crate::libsodium::memory_protection::MemoryProtectedBytes;
use crate::libsodium::{pwhash, secretbox};
use crate::UnprotectedKey;
use crate::{CryptoError, Result};
use libsodium_sys as ls;

use serde::{Deserialize, Serialize};

#[derive(Debug, PartialEq, Eq, Hash, Clone, Serialize, Deserialize)]
pub struct ProtectedKey {
    pub key_type: KeyType,
    pub salt: pwhash::Salt,
    pub encrypted_key: secretbox::SecretBox,
    pub hash_parameters: HashParameters,
}

#[derive(Debug, PartialEq, Eq, Hash, Clone, Serialize, Deserialize)]
pub enum KeyType {
    SecretBox,
}

#[derive(Debug, PartialEq, Eq, Hash, Clone, Serialize, Deserialize)]
pub enum HashParameters {
    Sensitive,
    Interactive,
}

impl ProtectedKey {
    pub fn unprotect(&self, password: MemoryProtectedBytes) -> Result<UnprotectedKey> {
        let pw_derived_key =
            secretbox::Key::new_from_password(password, &self.salt, &self.hash_parameters)?;
        Ok(match self.key_type {
            KeyType::SecretBox => {
                let mut unprotected_key = secretbox::Key::new_empty();
                let key_bytes: &mut [u8] = unprotected_key.as_mut();
                self.encrypted_key
                    .decrypt_into(&pw_derived_key, key_bytes)?;
                UnprotectedKey::SecretBox(unprotected_key)
            }
        })
    }

    pub fn to_printable_string(&self, comment: &Option<String>) -> Result<String> {
        let bytes = rmp_serde::to_vec(&self)?;
        encode("bs-protected-key", comment, &bytes)
    }

    pub fn from_printable_string(data: impl AsRef<str>) -> Result<Self> {
        let bytes = decode("bs-protected-key", data)?;
        Ok(rmp_serde::from_read_ref(&bytes)?)
    }

    pub fn fingerprint(&self) -> Result<KeyFingerprint> {
        KeyFingerprint::compute(&self)
    }
}

#[derive(PartialEq, Eq, Hash, Debug, Clone)]
pub struct KeyFingerprint([u8; FINGERPRINT_SIZE]);

const FINGERPRINT_SIZE: usize = 32;

impl KeyFingerprint {
    fn compute_from_bytes(protected_key_bytes: &[u8]) -> Result<Self> {
        let mut out = [0u8; FINGERPRINT_SIZE];
        unsafe {
            let res = ls::crypto_generichash(
                out.as_mut_ptr(),
                FINGERPRINT_SIZE,
                protected_key_bytes.as_ptr(),
                protected_key_bytes.len() as u64,
                std::ptr::null(),
                0,
            );
            if res != 0 {
                Err(CryptoError::SodiumError("generichash failed".into()))
            } else {
                Ok(KeyFingerprint(out))
            }
        }
    }

    pub fn compute(protected_key: &ProtectedKey) -> Result<Self> {
        let bytes = rmp_serde::to_vec(protected_key)?;
        KeyFingerprint::compute_from_bytes(&bytes)
    }

    pub fn to_hex_string(&self) -> String {
        let digest_bytes: &[u8] = &self.0.as_ref();
        let mut out = String::with_capacity(2 * digest_bytes.len());
        for byte in digest_bytes {
            out.push_str(&format!("{:x}", byte));
        }
        out
    }
}
