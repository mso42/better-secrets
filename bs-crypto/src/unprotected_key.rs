use crate::libsodium::memory_protection::MemoryProtectedBytes;
use crate::libsodium::pwhash::Salt;
use crate::libsodium::secretbox;
use crate::Result;
use crate::{HashParameters, KeyType, ProtectedKey};

#[derive(Debug)]
pub enum UnprotectedKey {
    SecretBox(secretbox::Key),
}

impl UnprotectedKey {
    pub fn protect(
        self,
        password: MemoryProtectedBytes,
        hash_parameters: HashParameters,
    ) -> Result<ProtectedKey> {
        let salt = Salt::random();
        let pw_derived_key = secretbox::Key::new_from_password(password, &salt, &hash_parameters)?;
        let (key_type, key_bytes) = match &self {
            UnprotectedKey::SecretBox(k) => (KeyType::SecretBox, k.as_ref()),
        };
        let encrypted_key = secretbox::SecretBox::encrypt(key_bytes, &pw_derived_key);
        Ok(ProtectedKey {
            key_type,
            salt,
            encrypted_key,
            hash_parameters,
        })
    }
}
