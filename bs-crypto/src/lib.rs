pub mod error;
pub mod protected_key;
pub mod unprotected_key;

pub use error::*;
pub use libsodium::memory_protection::MemoryProtectedBytes;
pub use libsodium::secretbox::{Key as SecretBoxKey, SecretBox};
pub use protected_key::*;
pub use unprotected_key::*;

mod internal;
mod libsodium;

pub fn init() -> error::Result<()> {
    libsodium::init()
}
