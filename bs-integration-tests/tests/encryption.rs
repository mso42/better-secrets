use std::io::Write;

use proptest::prelude::*;
use tempfile::NamedTempFile;

#[cfg(test)]
mod common;
use common::*;

proptest! {
    #![proptest_config(ProptestConfig::with_cases(16))]

    #[test]
    fn it_fails_with_no_output_if_wrong_password(key_password in ".*", encryption_password in ".*", data: Vec<u8>) {
        prop_assume!(key_password != encryption_password);
        let key_path = make_key_with_password(&key_password);

        let mut data_file = NamedTempFile::new().unwrap();
        data_file.write_all(&data).unwrap();
        let data_path = data_file.into_temp_path();

        let mut child = spawn_encrypt(&key_path, &data_path);
        let mut stdin = child.stdin.take().unwrap();

        write!(stdin, "{}\n", encryption_password).unwrap();

        let output = child.wait_with_output().unwrap();
        drop(key_path);
        drop(data_path);

        prop_assert!(!output.status.success());
        prop_assert!(output.stdout.is_empty());

        // Some message on stderr
        prop_assert!(!output.stderr.is_empty());
    }

    #[test]
    fn it_succeeds_with_some_output(password in ".*", data: Vec<u8>) {
        let key_path = make_key_with_password(&password);

        let mut data_file = NamedTempFile::new().unwrap();
        data_file.write_all(&data).unwrap();
        let data_path = data_file.into_temp_path();

        let mut child = spawn_encrypt(&key_path, &data_path);
        let mut stdin = child.stdin.take().unwrap();

        write!(stdin, "{}\n", password).unwrap();

        let output = child.wait_with_output().unwrap();
        drop(key_path);
        drop(data_path);

        prop_assert!(output.status.success());

        // Some message on stderr
        prop_assert!(!output.stdout.is_empty());
    }
}
