use std::env::{current_exe, var};
use std::ffi::OsStr;
use std::fs::read as read_file;
use std::io::Write;
use std::path::Path;
use std::process::Child;
use std::process::{Command, Stdio};

use tempfile::{NamedTempFile, TempPath};

pub fn target_command(binary: &OsStr) -> Command {
    let mut path = current_exe().unwrap();
    path.pop();
    debug_assert_eq!(path.file_name(), Some(OsStr::new("deps")));
    path.pop();
    debug_assert_eq!(path.file_name(), Some(OsStr::new("debug")));
    path.push(binary);
    let mut command = Command::new(&path);
    command
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .stderr(Stdio::piped());
    command
}

#[allow(dead_code)]
pub fn chonky_tests() -> bool {
    match var("CHONKY_TESTS").map(|s| s.to_lowercase()).as_deref() {
        Ok("1") => true,
        Ok("yes") => true,
        Ok("true") => true,
        _ => false,
    }
}

#[allow(dead_code)]
pub fn spawn_encrypt(key: &Path, data: &Path) -> Child {
    let mut command = target_command(OsStr::new("bs"));
    command.arg("encrypt").arg("--key").arg(&key).arg(&data);
    command.spawn().unwrap()
}

#[allow(dead_code)]
pub fn make_key_with_password(password: &str) -> TempPath {
    let key_path = NamedTempFile::new().unwrap().into_temp_path();
    let mut generator = target_command(OsStr::new("bs"))
        .arg("gen-key")
        .arg("--interactive")
        .arg("--out")
        .arg(&key_path)
        .spawn()
        .unwrap();
    let mut stdin = generator.stdin.take().unwrap();
    write!(stdin, "{}\n{}\n", password, password).unwrap();
    let exit_status = generator.wait().unwrap();
    debug_assert!(exit_status.success());
    debug_assert_ne!(read_file(&key_path).unwrap(), b"");
    key_path
}
