use std::ffi::{OsStr, OsString};
use std::io::Write;
use std::process::Child;

use proptest::prelude::*;

#[cfg(test)]
mod common;
#[cfg(test)]
use common::*;

fn spawn_gen_key(comment: &Option<OsString>) -> Child {
    let comment_args: Vec<&OsStr> = match *comment {
        None => vec![],
        Some(ref c) => vec![&OsStr::new("--comment"), &c],
    };
    let interactive_args: Vec<&OsStr> = match chonky_tests() {
        true => vec![],
        false => vec![&OsStr::new("--interactive")],
    };
    target_command(OsStr::new("bs"))
        .arg("gen-key")
        .args(&interactive_args)
        .args(&comment_args)
        .spawn()
        .unwrap()
}

proptest! {
    #![proptest_config(ProptestConfig::with_cases(16))]

    #[test]
    fn it_fails_with_no_output_if_passwords_dont_match(password1 in ".*", password2 in ".*", comment: Option<OsString>) {
        prop_assume!(password1 != password2);
        let mut child = spawn_gen_key(&comment);
        let mut stdin = child.stdin.take().unwrap();

        write!(stdin, "{}\n", password1).unwrap();
        write!(stdin, "{}\n", password2).unwrap();

        let output = child.wait_with_output().unwrap();

        prop_assert!(!output.status.success());
        prop_assert!(output.stdout.is_empty());

        // Some message on stderr
        prop_assert!(!output.stderr.is_empty());
    }

    #[test]
    fn it_generates_a_key(password: String, comment: Option<OsString>) {
        let mut child = spawn_gen_key(&comment);
        let mut stdin = child.stdin.take().unwrap();

        write!(stdin, "{}\n", password).unwrap();
        write!(stdin, "{}\n", password).unwrap();

        let output = child.wait_with_output().unwrap();

        prop_assert!(output.status.success());
        prop_assert!(output.stderr.starts_with(b"Key fingerprint: "));

        // Check output is valid UTF-8
        let output_string = String::from_utf8(output.stdout);
        prop_assert!(output_string.is_ok());
        prop_assert!(output_string.unwrap().starts_with("bs-protected-key\n"));
    }
}
