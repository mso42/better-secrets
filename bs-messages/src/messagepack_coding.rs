use rmp_serde::decode as rmp_decode;
use rmp_serde::encode as rmp_encode;
use serde::{Deserialize, Serialize};

pub trait Coding<'de> {
    type Output;
    fn encode(&self) -> std::result::Result<Box<[u8]>, rmp_encode::Error>;
    fn decode(buf: &'de [u8]) -> std::result::Result<Self::Output, rmp_decode::Error>;
}

impl<'de, T> Coding<'de> for T
where
    T: Serialize + Deserialize<'de>,
{
    type Output = Self;
    fn decode(buf: &'de [u8]) -> std::result::Result<Self, rmp_decode::Error> {
        rmp_decode::from_read_ref(buf)
    }

    fn encode(&self) -> std::result::Result<Box<[u8]>, rmp_encode::Error> {
        rmp_encode::to_vec(&self).map(|v| v.into())
    }
}

pub type EncodingError = rmp_encode::Error;
pub type DecodingError = rmp_decode::Error;
