use bs_crypto::{ProtectedKey, SecretBox};

use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub enum Request {
    AddKey {
        protected_key: ProtectedKey,
    },
    Encrypt {
        protected_key: ProtectedKey,
        payload: Box<[u8]>,
    },
    Decrypt {
        protected_key: ProtectedKey,
        secret_box: SecretBox,
    },
}
