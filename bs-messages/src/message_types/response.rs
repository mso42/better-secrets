use bs_crypto::SecretBox;

use serde::{Deserialize, Serialize};
use thiserror::Error;

#[derive(Debug, Error, Serialize, Deserialize)]
pub enum ResponseError {
    #[error("{0}")]
    BadRequest(String),
    #[error("Access denied")]
    AccessDenied,
    #[error("{0}")]
    InternalError(String),
    #[error("Key not found")]
    KeyNotFound,
}

#[derive(Serialize, Deserialize)]
pub enum ResponseOk {
    Ok,
    SecretBox(SecretBox),
    DecryptedData(Box<[u8]>),
}

pub type Result<T> = std::result::Result<T, ResponseError>;

pub type Response = Result<ResponseOk>;
