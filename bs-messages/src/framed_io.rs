use std::convert::TryInto;
use std::fmt::Debug;
use std::io;
use std::io::{Read, Write};

use thiserror::Error;

#[derive(Debug, Error)]
pub enum LengthDelimitedStreamError {
    #[error(transparent)]
    IoError(#[from] io::Error),
    #[error("Message too long (max message size = {0})")]
    MessageTooLong(usize),
    #[error("Architecture usize too small")]
    UsizeTooSmall,
}

type Result<T> = std::result::Result<T, LengthDelimitedStreamError>;

pub trait LengthDelimitedRead {
    fn read_message(&mut self) -> Result<Box<[u8]>>;
}

impl<R> LengthDelimitedRead for R
where
    R: Read,
{
    fn read_message(&mut self) -> Result<Box<[u8]>> {
        let mut length_bytes = [0u8; 4];
        self.read_exact(&mut length_bytes)?;
        let length: usize = u32::from_be_bytes(length_bytes)
            .try_into()
            .map_err(|_| LengthDelimitedStreamError::UsizeTooSmall)?;
        let mut data = vec![0u8; length];
        self.read_exact(&mut data)?;
        Ok(data.into())
    }
}

pub trait LengthDelimitedWrite {
    fn write_message(&mut self, message: impl AsRef<[u8]>) -> Result<()>;
}

impl<W> LengthDelimitedWrite for W
where
    W: Write,
{
    fn write_message(&mut self, message: impl AsRef<[u8]>) -> Result<()> {
        let length_usize: usize = message.as_ref().len();
        let length: u32 = length_usize
            .try_into()
            .map_err(|_| LengthDelimitedStreamError::MessageTooLong(length_usize))?;
        let length_bytes: [u8; 4] = length.to_be_bytes();
        self.write_all(&length_bytes)?;
        self.write_all(message.as_ref())?;
        Ok(())
    }
}
