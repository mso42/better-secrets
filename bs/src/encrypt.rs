use crate::agent_client::message_agent;
use crate::read_password::read_password;

use bs_crypto::{ProtectedKey, SecretBox, UnprotectedKey};
use bs_messages::{Request, ResponseOk};

use anyhow::{anyhow, Result};

pub fn try_encrypt_with_agent(
    protected_key: &ProtectedKey,
    payload: impl AsRef<[u8]>,
) -> Result<SecretBox> {
    let request = Request::Encrypt {
        protected_key: protected_key.clone(),
        payload: payload.as_ref().to_vec().into(),
    };
    let response_ok = message_agent(&request)?;
    if let ResponseOk::SecretBox(sb) = response_ok {
        Ok(sb)
    } else {
        Err(anyhow!("Unexpected response variant"))
    }
}

pub fn try_encrypt_local(
    protected_key: &ProtectedKey,
    payload: impl AsRef<[u8]>,
) -> Result<SecretBox> {
    let fingerprint = protected_key.fingerprint()?.to_hex_string();
    let password = read_password(&format!(
        "Enter password for key with fingerprint {}: ",
        &fingerprint
    ))?;
    let unprotected_key = protected_key.unprotect(password)?;
    let UnprotectedKey::SecretBox(sb_key) = unprotected_key;
    let secret_box = SecretBox::encrypt(payload.as_ref(), &sb_key);
    Ok(secret_box)
}
