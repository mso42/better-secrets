use anyhow::Result;
use atty::Stream;
use bs_crypto::MemoryProtectedBytes;

pub fn read_password(prompt: &str) -> Result<MemoryProtectedBytes> {
    let password_string = if atty::is(Stream::Stdin) {
        rpassword::read_password_from_tty(Some(prompt))?
    } else {
        rpassword::read_password()?
    };
    Ok(password_string.into())
}
