use std::path::PathBuf;
use structopt::StructOpt;

#[derive(StructOpt, Debug)]
pub enum Options {
    /// Generate a key
    GenKey {
        #[structopt(
            long,
            help = "The key is intended for interactive use. This means that protecting/unprotecting it will be fast, at the expense of being more vulnerable to brute-force attacks."
        )]
        interactive: bool,
        #[structopt(short, long, help = "The file to write the generated key to")]
        key: PathBuf,
        #[structopt(short, long, help = "An optional comment to include in the key")]
        comment: Option<String>,
    },
    /// Encrypt a file using a key, reading stdin for the password, and outputting the encrypted data to stdout
    Encrypt {
        #[structopt(
            short,
            long,
            help = "The file to read the protected key from. Defaults to ~/.config/bs/user.key."
        )]
        key: Option<PathBuf>,
        #[structopt(help = "The file to encrypt.")]
        file: PathBuf,
    },
    /// Decrypt a file using a key, reading stdin for the password, and outputting the decrypted data to stdout
    Decrypt {
        #[structopt(
            short,
            long,
            help = "The file to read the protected key from. Defaults to ~/.config/bs/user.key."
        )]
        key: Option<PathBuf>,
        #[structopt(help = "The file to decrypt")]
        file: PathBuf,
    },
    /// Add a key to the agent
    AddKey {
        #[structopt(
            short,
            long,
            help = "The file to read the protected key from. Defaults to ~/.config/bs/user.key."
        )]
        key: Option<PathBuf>,
    },
}
