mod add_key;
mod agent_client;
mod cli;
mod decrypt;
mod encrypt;
mod read_password;

use add_key::try_add_key_to_agent;
use cli::Options;
use decrypt::{try_decrypt_local, try_decrypt_with_agent};
use encrypt::{try_encrypt_local, try_encrypt_with_agent};
use read_password::read_password;

use bs_crypto::{
    HashParameters, KeyFingerprint, MemoryProtectedBytes, ProtectedKey, SecretBox, SecretBoxKey,
    UnprotectedKey,
};
use bs_messages::Coding;

use std::env;
use std::fs::{read as read_file, write as write_file};
use std::io::{stderr, stdout, Write};
use std::path::{Path, PathBuf};
use std::str::from_utf8;

use anyhow::{anyhow, Result};
use log::info;
use structopt::StructOpt;

fn main() -> Result<()> {
    env_logger::init();
    bs_crypto::init()?;
    let options = Options::from_args();
    match options {
        Options::GenKey {
            interactive,
            comment,
            key,
        } => handle_gen_key(interactive, comment, key),
        Options::Encrypt { key, file } => handle_encrypt(key, file),
        Options::Decrypt { key, file } => handle_decrypt(key, file),
        Options::AddKey { key } => handle_add_key(key),
    }
}

fn handle_encrypt(key: Option<PathBuf>, file: PathBuf) -> Result<()> {
    let key_path = key.map(Result::Ok).unwrap_or_else(get_default_key_path)?;
    let protected_key = read_key_file(&key_path)?;
    let payload = read_file(&file)?;

    let encrypted = match try_encrypt_with_agent(&protected_key, &payload) {
        Ok(encrypted) => encrypted,
        Err(e) => {
            info!("Agent encryption failed: {}", e);
            try_encrypt_local(&protected_key, &payload)?
        }
    };
    stdout().write_all(&encrypted.encode()?)?;
    Ok(())
}

fn handle_decrypt(key: Option<PathBuf>, file: PathBuf) -> Result<()> {
    let key_path = key.map(Result::Ok).unwrap_or_else(get_default_key_path)?;
    let secret_box_bytes = read_file(&file)?;
    let secret_box = SecretBox::decode(&secret_box_bytes)?;
    let protected_key = read_key_file(&key_path)?;

    let decrypted = match try_decrypt_with_agent(&protected_key, &secret_box) {
        Ok(decrypted) => decrypted,
        Err(e) => {
            info!("Agent decryption failed: {}", e);
            try_decrypt_local(&protected_key, &secret_box)?
        }
    };
    stdout().write_all(decrypted.as_ref())?;

    Ok(())
}

fn handle_add_key(key: Option<PathBuf>) -> Result<()> {
    let key_path = key.map(Result::Ok).unwrap_or_else(get_default_key_path)?;
    let protected_key = read_key_file(&key_path)?;
    try_add_key_to_agent(&protected_key)?;
    Ok(())
}

fn handle_gen_key(interactive: bool, comment: Option<String>, key: PathBuf) -> Result<()> {
    let unprotected_key = UnprotectedKey::SecretBox(SecretBoxKey::new_random());
    let password = read_password_with_confirm()?;
    let hash_parameters = match interactive {
        false => HashParameters::Sensitive,
        true => HashParameters::Interactive,
    };
    let protected_key = unprotected_key.protect(password, hash_parameters)?;
    let printable_string = protected_key.to_printable_string(&comment)?;
    write_file(key, printable_string.as_bytes())?;
    let fingerprint = KeyFingerprint::compute(&protected_key)?;
    stderr().write_all(b"Key fingerprint: ")?;
    stderr().write_all(fingerprint.to_hex_string().as_bytes())?;
    stderr().write_all(b"\n")?;
    Ok(())
}

fn read_password_with_confirm() -> Result<MemoryProtectedBytes> {
    let password1 = read_password("Enter password: ")?;
    let password2 = read_password("Confirm password: ")?;
    if *password1 != *password2 {
        return Err(anyhow!("Passwords did not match"));
    }
    Ok(password1)
}

fn read_key_file(path: &Path) -> Result<ProtectedKey> {
    let key_data = read_file(path)?;
    let key_string = from_utf8(&key_data)?;
    let protected_key = ProtectedKey::from_printable_string(&key_string)?;
    Ok(protected_key)
}

fn get_default_key_path() -> Result<PathBuf> {
    let config_path = get_config_path()?;
    Ok([config_path, "bs".into(), "user.key".into()]
        .iter()
        .collect())
}

fn get_config_path() -> Result<PathBuf> {
    if let Some(config_path) = env::var_os("XDG_CONFIG_HOME") {
        return Ok(config_path.into());
    }
    if let Some(home_path) = env::var_os("HOME") {
        return Ok([home_path, ".config".into()].iter().collect());
    }
    Err(anyhow!("neither XDG_CONFIG_HOME nor HOME was set"))
}
