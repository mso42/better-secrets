use bs_messages::{
    Coding, LengthDelimitedRead, LengthDelimitedWrite, Request, Response, ResponseOk,
};

use anyhow::Result;

use std::env;
use std::os::unix::net::UnixStream;

const SOCKET_ENV_VAR_NAME: &str = "BS_AGENT_SOCK";

pub fn message_agent(request: &Request) -> Result<ResponseOk> {
    let socket_path = env::var(SOCKET_ENV_VAR_NAME)?;
    let mut stream = UnixStream::connect(socket_path)?;
    stream.write_message(request.encode()?)?;
    let response_bytes = stream.read_message()?;
    let response = Response::decode(&response_bytes)?;
    Ok(response?)
}
