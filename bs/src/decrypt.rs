use crate::agent_client::message_agent;
use crate::read_password::read_password;

use bs_crypto::{ProtectedKey, SecretBox, UnprotectedKey};
use bs_messages::{Request, ResponseOk};

use anyhow::{anyhow, Result};

pub fn try_decrypt_with_agent(
    protected_key: &ProtectedKey,
    secret_box: &SecretBox,
) -> Result<Box<[u8]>> {
    let request = Request::Decrypt {
        protected_key: protected_key.clone(),
        secret_box: secret_box.clone(),
    };
    let response_ok = message_agent(&request)?;
    if let ResponseOk::DecryptedData(data) = response_ok {
        Ok(data)
    } else {
        Err(anyhow!("Unexpected response variant"))
    }
}

pub fn try_decrypt_local(
    protected_key: &ProtectedKey,
    secret_box: &SecretBox,
) -> Result<Box<[u8]>> {
    let fingerprint = protected_key.fingerprint()?.to_hex_string();
    let password = read_password(&format!(
        "Enter password for key with fingerprint {}: ",
        &fingerprint
    ))?;
    let unprotected_key = protected_key.unprotect(password)?;
    let UnprotectedKey::SecretBox(sb_key) = unprotected_key;
    let mut decrypted: Box<[u8]> = vec![0; secret_box.data.len()].into();
    secret_box.decrypt_into(&sb_key, &mut decrypted)?;
    Ok(decrypted)
}
