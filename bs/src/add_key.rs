use crate::agent_client::message_agent;

use bs_crypto::ProtectedKey;
use bs_messages::{Request, ResponseOk};

use anyhow::{anyhow, Result};

pub fn try_add_key_to_agent(protected_key: &ProtectedKey) -> Result<()> {
    let request = Request::AddKey {
        protected_key: protected_key.clone(),
    };
    let response_ok = message_agent(&request)?;
    if let ResponseOk::Ok = response_ok {
        Ok(())
    } else {
        Err(anyhow!("Unexpected response variant"))
    }
}
