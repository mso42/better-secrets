use std::path::PathBuf;

use structopt::StructOpt;

#[derive(Debug, StructOpt)]
/// Run agent listening on a specified Unix socket. Daemonize using systemd. Use systemd ffs.
pub struct Options {
    #[structopt(
        short = "a",
        long = "address",
        help = "The path of the Unix socket to bind to"
    )]
    pub socket_path: PathBuf,
    #[structopt(
        short = "c",
        long = "confirm-program",
        help = "The path of a program to execute to get user confirmation for key use. The program should behave like ssh-askpass(1), i.e. take a prompt as its only argument, and return 0 iff the user confirmed. This should be an absolute path to avoid PATH-injection vulnerabilities. The default is /usr/lib/ssh/ssh-askpass."
    )]
    pub confirm_program_path: Option<PathBuf>,
}
