use std::path::PathBuf;
use std::process::Command;

use bs_crypto::{KeyFingerprint, MemoryProtectedBytes};

use log::warn;

pub trait Confirm {
    fn confirm(&self, key_fingerprint: &KeyFingerprint) -> std::io::Result<bool>;
}

pub trait PasswordPrompter {
    fn prompt(&self, key_fingerprint: &KeyFingerprint) -> std::io::Result<MemoryProtectedBytes>;
}

pub struct ProgramInteractor {
    path: PathBuf,
}

impl ProgramInteractor {
    pub fn new(path: PathBuf) -> Self {
        if path.is_relative() {
            warn!("Relative path specified for interactor program. Make sure you trust your PATH environment variable.")
        }
        ProgramInteractor { path }
    }
}

impl Confirm for ProgramInteractor {
    fn confirm(&self, key_fingerprint: &KeyFingerprint) -> std::io::Result<bool> {
        let output = Command::new(&self.path)
            .arg(format!(
                "Confirm use of key with fingerprint: {}",
                key_fingerprint.to_hex_string()
            ))
            .output()?;
        Ok(output.status.success())
    }
}

impl PasswordPrompter for ProgramInteractor {
    fn prompt(&self, key_fingerprint: &KeyFingerprint) -> std::io::Result<MemoryProtectedBytes> {
        let mut output = Command::new(&self.path)
            .arg(format!(
                "Enter password for key with fingerprint: {}",
                key_fingerprint.to_hex_string()
            ))
            .output()?;
        let success = output.status.success();
        if !success {
            return Err(std::io::Error::new(
                std::io::ErrorKind::Other,
                "command did not exit successfully",
            ));
        }
        if output.stdout.ends_with(&[b'\n']) {
            output.stdout.pop();
        }
        let output = MemoryProtectedBytes::from(output.stdout);
        Ok(output)
    }
}
