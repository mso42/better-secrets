use thiserror::Error;

use bs_crypto::CryptoError;
use bs_messages::{DecodingError, EncodingError, ResponseError};

#[derive(Debug, Error)]
pub enum AgentError {
    #[error("Access denied")]
    AccessDenied,
    #[error("Key not found")]
    KeyNotFound,
    #[error("agent io error: {0}")]
    IoError(#[from] std::io::Error),
    #[error("miscellaneous agent error: {0}")]
    InternalMiscError(String),
    #[error("decode error: {0}")]
    MessagePackDecodeError(#[from] DecodingError),
    #[error("encode error: {0}")]
    MessagePackEncodeError(#[from] EncodingError),
    #[error("crypto error: {0}")]
    CryptoError(#[from] CryptoError),
}

pub type Result<T> = std::result::Result<T, AgentError>;

impl From<AgentError> for ResponseError {
    fn from(err: AgentError) -> Self {
        match err {
            AgentError::AccessDenied => ResponseError::AccessDenied,
            AgentError::KeyNotFound => ResponseError::KeyNotFound,
            err => ResponseError::InternalError(format!("{}", err)),
        }
    }
}
