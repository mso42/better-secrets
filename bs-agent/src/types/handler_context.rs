use std::collections::HashMap;
use std::sync::{Arc, RwLock};

use log::info;

use bs_crypto::{ProtectedKey, UnprotectedKey};

use crate::types::error::{AgentError, Result};
use crate::user_interaction::{Confirm, PasswordPrompter};

#[derive(Clone)]
pub struct HandlerContext {
    confirm: Arc<dyn Confirm + Send + Sync>,
    password_prompter: Arc<dyn PasswordPrompter + Send + Sync>,
    keys: Arc<RwLock<HashMap<ProtectedKey, Arc<UnprotectedKey>>>>,
}

impl HandlerContext {
    pub fn new(
        confirm: impl Confirm + Send + Sync + 'static,
        password_prompter: impl PasswordPrompter + Send + Sync + 'static,
    ) -> Self {
        HandlerContext {
            confirm: Arc::new(confirm),
            password_prompter: Arc::new(password_prompter),
            keys: Arc::new(RwLock::new(HashMap::new())),
        }
    }

    pub fn get_key<'a>(&self, protected_key: &'a ProtectedKey) -> Result<GatedKey<'a>> {
        let guard = self.keys.read().unwrap();
        let unprotected_key = guard
            .get(protected_key)
            .ok_or_else(|| not_found(protected_key))?
            .clone();
        drop(guard);
        Ok(GatedKey::new(
            protected_key,
            unprotected_key,
            self.confirm.clone(),
        ))
    }

    pub fn add_key(&self, protected_key: ProtectedKey) -> Result<()> {
        let password = self
            .password_prompter
            .prompt(&protected_key.fingerprint()?)?;
        let unprotected_key = protected_key.unprotect(password)?;
        let mut guard = self.keys.write().unwrap();
        guard.insert(protected_key, Arc::new(unprotected_key));
        drop(guard);
        Ok(())
    }
}

pub struct GatedKey<'a> {
    protected_key: &'a ProtectedKey,
    unprotected_key: Arc<UnprotectedKey>,
    gate: Arc<dyn Confirm>,
}

impl<'a> GatedKey<'a> {
    pub fn new(
        protected_key: &'a ProtectedKey,
        unprotected_key: Arc<UnprotectedKey>,
        gate: Arc<dyn Confirm>,
    ) -> Self {
        GatedKey {
            protected_key,
            unprotected_key,
            gate,
        }
    }

    pub fn acquire(&self) -> Result<&UnprotectedKey> {
        if self.gate.confirm(&self.protected_key.fingerprint()?)? {
            Ok(&self.unprotected_key)
        } else {
            Err(access_denied(&self.protected_key))
        }
    }
}

fn not_found(protected_key: &ProtectedKey) -> AgentError {
    let fingerprint_string = protected_key
        .fingerprint()
        .map(|f| f.to_hex_string())
        .unwrap_or_else(|err| format!("{}", err));
    info!("key not found: {}", fingerprint_string);
    AgentError::KeyNotFound
}

fn access_denied(protected_key: &ProtectedKey) -> AgentError {
    let fingerprint_string = protected_key
        .fingerprint()
        .map(|f| f.to_hex_string())
        .unwrap_or_else(|err| format!("{}", err));
    info!("access denied: {}", fingerprint_string);
    AgentError::AccessDenied
}
