use crate::types::error::Result;
use crate::types::handler_context::HandlerContext;

use bs_crypto::{ProtectedKey, SecretBox, UnprotectedKey};
use bs_messages::{Request, ResponseOk};

use log::debug;

pub fn handle_request(request: Request, context: HandlerContext) -> Result<ResponseOk> {
    match request {
        Request::AddKey { protected_key } => handle_add_key(protected_key, context),
        Request::Encrypt {
            protected_key,
            payload,
        } => handle_encrypt(protected_key, payload, context),
        Request::Decrypt {
            protected_key,
            secret_box,
        } => handle_decrypt(protected_key, secret_box, context),
    }
}

fn handle_add_key(protected_key: ProtectedKey, context: HandlerContext) -> Result<ResponseOk> {
    debug!("add-key request");
    context.add_key(protected_key)?;
    Ok(ResponseOk::Ok)
}

fn handle_encrypt(
    protected_key: ProtectedKey,
    payload: Box<[u8]>,
    context: HandlerContext,
) -> Result<ResponseOk> {
    debug!("encryption request");
    let gated_key = context.get_key(&protected_key)?;
    let unprotected_key = gated_key.acquire()?;
    let UnprotectedKey::SecretBox(sb_key) = unprotected_key;
    let encrypted = SecretBox::encrypt(payload.as_ref(), &sb_key);
    Ok(ResponseOk::SecretBox(encrypted))
}

fn handle_decrypt(
    protected_key: ProtectedKey,
    secret_box: SecretBox,
    context: HandlerContext,
) -> Result<ResponseOk> {
    debug!("decryption request");
    let gated_key = context.get_key(&protected_key)?;
    let unprotected_key = gated_key.acquire()?;
    let UnprotectedKey::SecretBox(sb_key) = unprotected_key;
    let mut decrypted: Box<[u8]> = vec![0; secret_box.data.len()].into();
    secret_box.decrypt_into(&sb_key, &mut decrypted)?;
    Ok(ResponseOk::DecryptedData(decrypted))
}
