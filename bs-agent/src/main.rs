mod cli;
mod request_handling;
mod types;
mod user_interaction;

use bs_messages::{Coding, LengthDelimitedRead, LengthDelimitedWrite, Request, ResponseError};

use cli::Options;
use request_handling::handle_request;
use structopt::StructOpt;
use types::handler_context::HandlerContext;

use std::fs::remove_file;
use std::os::unix::net::UnixListener;
use std::path::{Path, PathBuf};
use std::process::exit;
use std::thread;

use anyhow::Result;
use log::{error, info};
use signal_hook::{register, SIGINT, SIGTERM};

fn main() -> Result<()> {
    env_logger::init();
    bs_crypto::init()?;
    info!("Agent starting");
    let options = Options::from_args();
    let confirm_program_path = options
        .confirm_program_path
        .unwrap_or_else(|| PathBuf::from("/usr/lib/ssh/ssh-askpass"));
    let confirm = user_interaction::ProgramInteractor::new(confirm_program_path.clone());
    let password_prompter = user_interaction::ProgramInteractor::new(confirm_program_path);
    let context = HandlerContext::new(confirm, password_prompter);
    info!("Listening on Unix socket at {:?}", options.socket_path);
    register_cleanup(&options.socket_path)?;

    let listener = UnixListener::bind(&options.socket_path)?;
    for stream in listener.incoming() {
        let stream = match stream {
            Ok(s) => s,
            Err(e) => {
                error!("Error opening stream: {}", e);
                continue;
            }
        };
        let context_owned = context.clone();
        thread::spawn(|| match handle_connection(stream, context_owned) {
            Ok(()) => info!("Handler completed successfully"),
            Err(e) => error!("Error in handler: {}", e),
        });
    }
    Ok(())
}

fn handle_connection(
    mut stream: impl LengthDelimitedRead + LengthDelimitedWrite,
    context: HandlerContext,
) -> Result<()> {
    let request_bytes = stream.read_message()?;
    let request = Request::decode(&request_bytes)?;
    let response = handle_request(request, context).map_err(ResponseError::from);
    let response_bytes = response.encode()?;
    stream.write_message(&response_bytes)?;
    Ok(())
}

fn register_cleanup(socket_path: &Path) -> Result<(), std::io::Error> {
    let socket_path_owned_1 = socket_path.to_owned();
    let socket_path_owned_2 = socket_path.to_owned();
    unsafe {
        register(SIGINT, move || {
            info!("Caught SIGINT. Cleaning up...");
            remove_socket(&socket_path_owned_1);
            exit(1)
        })?;
        register(SIGTERM, move || {
            info!("Caught SIGTERM. Cleaning up...");
            remove_socket(&socket_path_owned_2);
            exit(1)
        })?;
    }
    Ok(())
}

fn remove_socket(socket_path: &Path) {
    info!("Removing socket {:?}", socket_path);
    let remove_result = remove_file(socket_path);
    if let Err(e) = remove_result {
        error!("Error removing socket file {:?}: {}", socket_path, e);
    }
}
